# 学生管理系统

#### 介绍

1. C语言进阶、C++入门
1. 文件操作函数、图形界面操作、数据处理

#### 编译环境

VScode g++（TDM-GCC-64 9.2.0）

#### 特技
![输入图片说明](image%E5%88%9D%E5%A7%8B%E9%A1%B5%E9%9D%A2.png)
1.  存储数据
2.  图形页面
3.  功能强大
4.  使用轻松

#### 安装教程

1.  将文件夹下载到 VScode 的 code 目录下。
2.  如果编译器的版本不一致，需要自备配置文件
3. （TDM-GCC-64 9.2.0）下载路径：[官网下载 tdm64-gcc-9.2.0.exe](https://github.com/jmeubank/tdm-gcc/releases/download/v9.2.0-tdm64-1/tdm64-gcc-9.2.0.exe) 或 [镜像站下载 tdm64-gcc-9.2.0.exe](https://github.91chi.fun/https://github.com//jmeubank/tdm-gcc/releases/download/v9.2.0-tdm64-1/tdm64-gcc-9.2.0.exe) 均可（57.6M）
4.  [配置VScode的参考链接](https://www.cnblogs.com/bpf-1024/p/11597000.html)

#### 文件目录

```
    VScode
      ├ AutoVsCEnv_WPF V1.994		    // VScode	
      ├ code
      │    └ studClass---Myedit
      │       ├ .vscode                     // 配置文件 
      │	      └ 源文件			    // 省略详细
      └ TDM-GCC-64                          // 编译器
```

#### 源文件描述

1.  base.cpp---输出基类、数据传递函数
2.  check.cpp---文件操作函数（实现账号密码检测）
3.  control.cpp---用户操作函数、冒泡排序
4.  CreatUI.cpp---图形页面绘制函数
5.  InputORoutputdata.cpp---文件数据处理函数
6.  main.cpp---主函数
7.  ClassData.txt---课程数据
8.  UserData.txt---用户数据（账号、密码）
9.  READER.txt---修改日记

#### 使用说明

1.  默认终端应用程序 ： Windows控制台主机（初始大小100*40）
2.  初始账号：123  初始密码：123；管理员：root 123
