#pragma once
#include "stdio.h"
#include <windows.h>
#include <conio.h>
#include <string.h>
#include "base.h"
#include "control.h"

int AddUserClass(char* name, int classnumber, char Usertxt[1024], int credit);
int DeleteUserClass(char* name, int classnumber, char Usertxt[1024], int credit);
int ReadClassData(char Classtxt[1024]);
int GetUserClass(char Usertxt[1024]);
int ClassDataInit(char Classtxt[1024]);
int UserDataInit(char Usertxt[1024]);
int ReadClassSelecter(int number, char Usertxt[1024], char ClassSelecter[16][16]);
int EditClass(int classnb, int listnb, char Classtxt[1024], char EditNote[]);
int SaveData(const char* name, char  txt[1024]);