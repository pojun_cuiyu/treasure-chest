#include "CreatUI.h"
// 绘画开始窗口
void CreatStartUI()
{
    // Ui.PaintConstString(开始输出的横坐标,开始输出的横坐标 ,输出的制表符);---整行输出
    Ui.PaintConstString(25, 10, "╔════════════════════════════════════════════════╗");
    // 整列输出
    for (int i = 0; i < 13; i++)
    {
        Ui.PaintConstString(25, i + 11, "║");
    }
    Ui.PaintConstString(25, 24, "╚════════════════════════════════════════════════╝");
    for (int i = 0; i < 13; i++)
    {
        Ui.PaintConstString(74, i + 11, "║");
    }
    Ui.PaintConstString(40, 12, "五邑小学学生选课系统");
    Ui.PaintConstString(36, 15, "账号：");
    Ui.PaintConstString(41, 16, "──────────────────");

    Ui.PaintConstString(36, 18, "密码：");
    Ui.PaintConstString(41, 19, "──────────────────");
    Ui.PaintConstString(46, 21, "→登录");
    Ui.PaintConstString(0, 38, "════════════════════════════════════════════════════════════════════════════════════════════════════");
}
void CreatUserUI()
{
    Ui.PaintConstString(0, 4, "════════════════════════════════════════════════════════════════════════════════════════════════════");
    for (int i = 0; i < 38; i++)
    {
        Ui.PaintConstString(9, i, "║");
    }
    Ui.PaintConstString(9, 4, "╬");
    Ui.PaintConstString(0, 38, "════════════════════════════════════════════════════════════════════════════════════════════════════");
    for (int i = 0; i < 4; i++)
    {
        Ui.PaintConstString(85, i, "║");
    }
    for (int i = 0; i < 4; i++)
    {
        Ui.PaintConstString(70, i, "║");
    }
    Ui.PaintConstString(0, 10, "═════════");
    Ui.PaintConstString(0, 16, "═════════");
    Ui.PaintConstString(0, 1, "   Wuyi ");
    Ui.PaintConstString(0, 2, " Primary");
    Ui.PaintConstString(0, 3, " School");
    Ui.PaintConstString(10, 3, " 欢迎使用选课系统。");
    Ui.PaintConstString(10, 1, "                 ，你好！");
    Ui.PaintConstString(89, 2, "退出系统");
    Ui.PaintConstString(74, 2, "退出登录");
    Ui.PaintConstString(3, 7, "选课");
    Ui.PaintConstString(1, 13, "我的课程");
    Ui.PaintConstString(2, 25, "广告位");
    Ui.PaintConstString(3, 26, "招租");
}
void CreatRootUI()
{
    Ui.PaintConstString(0, 4, "════════════════════════════════════════════════════════════════════════════════════════════════════");
    for (int i = 0; i < 38; i++)
    {
        Ui.PaintConstString(9, i, "║");
    }
    Ui.PaintConstString(9, 4, "╬");
    Ui.PaintConstString(0, 38, "════════════════════════════════════════════════════════════════════════════════════════════════════");
    for (int i = 0; i < 4; i++)
    {

        Ui.PaintConstString(85, i, "║");
    }
    for (int i = 0; i < 4; i++)
    {
        Ui.PaintConstString(70, i, "║");
    }
    Ui.PaintConstString(0, 10, "═════════");
    Ui.PaintConstString(0, 16, "═════════");
    Ui.PaintConstString(0, 1, "   Wuyi ");
    Ui.PaintConstString(0, 2, " Primary");
    Ui.PaintConstString(0, 3, " School");
    Ui.PaintConstString(10, 3, " 欢迎使用选课管理系统。");
    Ui.PaintConstString(10, 1, "                 ，你好！");
    Ui.PaintConstString(89, 2, "退出系统");
    Ui.PaintConstString(74, 2, "退出登录");
    Ui.PaintConstString(1, 7, "所有课程");
    Ui.PaintConstString(1, 13, "管理课程");
    Ui.PaintConstString(2, 25, "广告位");
    Ui.PaintConstString(3, 26, "招租");
}
void CreatUserUI_Class()
{
    Ui.PaintConstString(11, 5, "╔═════╦═════════════════════╦═══════════╦═══════╦═════════╦═══════════════╦═════╦═════╗");
    for (int i = 6; i < 38; i++)
    {
        Ui.PaintConstString(11, i, "║");
    }
    Ui.PaintConstString(13, 6, "编号║         名称        ║    性质   ║ 总学时║ 授课学时║ 实验或上机学时║ 学分║ 容量║");

    // Ui.PaintConstString(11, 7, "╟═════════════════════════════════════════════════════════════════════════════════════╢");
    Ui.PaintConstString(11, 7, "╟═════╬═════════════════════╬═══════════╬═══════╬═════════╬═══════════════╬═════╬═════╢");

    for (int i = 8; i < 20; i += 2) /////////画出分割线，i是行数
    {
        Ui.PaintConstString(17, i, "║");
        Ui.PaintConstString(39, i, "║");
        Ui.PaintConstString(51, i, "║");
        Ui.PaintConstString(59, i, "║");
        Ui.PaintConstString(69, i, "║");
        Ui.PaintConstString(85, i, "║");
        Ui.PaintConstString(91, i, "║");
        Ui.PaintConstString(97, i, "║");
        Ui.PaintConstString(11, i + 1, "╟═════╬═════════════════════╬═══════════╬═══════╬═════════╬═══════════════╬═════╬═════╢");
    }
    Ui.PaintConstString(17, 20, "║");
    Ui.PaintConstString(39, 20, "║");
    Ui.PaintConstString(51, 20, "║");
    Ui.PaintConstString(59, 20, "║");
    Ui.PaintConstString(69, 20, "║");
    Ui.PaintConstString(85, 20, "║");
    Ui.PaintConstString(91, 20, "║");
    Ui.PaintConstString(97, 20, "║");
    Ui.PaintConstString(11, 21, "╟═════╩═════════════════════╩═══════════╩═══════╩═════════╩═══════════════╩═════╩═════╝");
    Ui.PaintConstString(11, 22, "╟═════════╗");
    Ui.PaintConstString(12, 23, " 课程内容║");
    Ui.PaintConstString(11, 24, "╟═════════════════════════════════════════════════════════════════════════════════════╗");
}
void CreatRootUI_Class()
{
    Ui.PaintConstString(11, 5, "╔═════╦═════════════════════╦═══════════╦═══════╦═════════╦═══════════════╦═════╦═════╗");
    for (int i = 6; i < 38; i++)
    {

        Ui.PaintConstString(11, i, "║");
    }
    Ui.PaintConstString(13, 6, "编号║         名称        ║    性质   ║ 总学时║ 授课学时║ 实验或上机学时║ 学分║ 容量║");

    // Ui.PaintConstString(11, 7, "╟═════════════════════════════════════════════════════════════════════════════════════╢");
    Ui.PaintConstString(11, 7, "╟═════╬═════════════════════╬═══════════╬═══════╬═════════╬═══════════════╬═════╬═════╢");

    for (int i = 8; i < 20; i += 2) /////////画出分割线，i是行数
    {
        Ui.PaintConstString(17, i, "║");
        Ui.PaintConstString(39, i, "║");
        Ui.PaintConstString(51, i, "║");
        Ui.PaintConstString(59, i, "║");
        Ui.PaintConstString(69, i, "║");
        Ui.PaintConstString(85, i, "║");
        Ui.PaintConstString(91, i, "║");
        Ui.PaintConstString(97, i, "║");
        Ui.PaintConstString(11, i + 1, "╟═════╬═════════════════════╬═══════════╬═══════╬═════════╬═══════════════╬═════╬═════╢");
    }
    Ui.PaintConstString(17, 20, "║");
    Ui.PaintConstString(39, 20, "║");
    Ui.PaintConstString(51, 20, "║");
    Ui.PaintConstString(59, 20, "║");
    Ui.PaintConstString(69, 20, "║");
    Ui.PaintConstString(85, 20, "║");
    Ui.PaintConstString(91, 20, "║");
    Ui.PaintConstString(97, 20, "║");
    Ui.PaintConstString(11, 21, "╟═════╩═════════════════════╩═══════════╩═══════╩═════════╩═══════════════╩═════╩═════╝");
    Ui.PaintConstString(11, 22, "╟═════════╗");
    Ui.PaintConstString(12, 23, " 课程内容║");
    Ui.PaintConstString(11, 24, "╟═════════════════════════════════════════════════════════════════════════════════════╗");
}
void CreatRootUI_AllClass()
{
    Ui.PaintConstString(11, 5, "╔═════╦═════════════════════╦═══════════╦═══════╦═════════╦═══════════════╦═════╦═════╗");
    for (int i = 6; i < 38; i++)
    {

        Ui.PaintConstString(11, i, "║");
    }
    Ui.PaintConstString(13, 6, "编号║         名称        ║    性质   ║ 总学时║ 授课学时║ 实验或上机学时║ 学分║ 容量║");

    // Ui.PaintConstString(11, 7, "╟═════════════════════════════════════════════════════════════════════════════════════╢");
    Ui.PaintConstString(11, 7, "╟═════╬═════════════════════╬═══════════╬═══════╬═════════╬═══════════════╬═════╬═════╢");

    for (int i = 8; i < 20; i += 2) /////////画出分割线，i是行数
    {
        Ui.PaintConstString(17, i, "║");
        Ui.PaintConstString(39, i, "║");
        Ui.PaintConstString(51, i, "║");
        Ui.PaintConstString(59, i, "║");
        Ui.PaintConstString(69, i, "║");
        Ui.PaintConstString(85, i, "║");
        Ui.PaintConstString(91, i, "║");
        Ui.PaintConstString(97, i, "║");
        Ui.PaintConstString(11, i + 1, "╟═════╬═════════════════════╬═══════════╬═══════╬═════════╬═══════════════╬═════╬═════╢");
    }
    Ui.PaintConstString(17, 20, "║");
    Ui.PaintConstString(39, 20, "║");
    Ui.PaintConstString(51, 20, "║");
    Ui.PaintConstString(59, 20, "║");
    Ui.PaintConstString(69, 20, "║");
    Ui.PaintConstString(85, 20, "║");
    Ui.PaintConstString(91, 20, "║");
    Ui.PaintConstString(97, 20, "║");
    Ui.PaintConstString(11, 21, "╟═════╩═════════════════════╩═══════════╩═══════╩═════════╩═══════════════╩═════╩═════╝");
    Ui.PaintConstString(11, 22, "╟═════════╗");
    Ui.PaintConstString(12, 23, " 选课情况║");
    Ui.PaintConstString(11, 24, "╟═════════════════════════════════════════════════════════════════════════════════════╗");
}
void CreatRootUI_ManageClass()
{
    Ui.PaintConstString(11, 5, "╔═════╦═════════════════════╦═══════════╦═══════╦═════════╦═══════════════╦═════╦═════╗");
    for (int i = 6; i < 38; i++)
    {
        Ui.PaintConstString(11, i, "║");
    }
    Ui.PaintConstString(13, 6, "编号║         名称        ║    性质   ║ 总学时║ 授课学时║ 实验或上机学时║ 学分║ 容量║");

    // Ui.PaintConstString(11, 7, "╟═════════════════════════════════════════════════════════════════════════════════════╢");
    Ui.PaintConstString(11, 7, "╟═════╬═════════════════════╬═══════════╬═══════╬═════════╬═══════════════╬═════╬═════╢");

    for (int i = 8; i < 20; i += 2) /////////画出分割线，i是行数
    {
        Ui.PaintConstString(17, i, "║");
        Ui.PaintConstString(39, i, "║");
        Ui.PaintConstString(51, i, "║");
        Ui.PaintConstString(59, i, "║");
        Ui.PaintConstString(69, i, "║");
        Ui.PaintConstString(85, i, "║");
        Ui.PaintConstString(91, i, "║");
        Ui.PaintConstString(97, i, "║");
        Ui.PaintConstString(11, i + 1, "╟═════╬═════════════════════╬═══════════╬═══════╬═════════╬═══════════════╬═════╬═════╢");
    }
    Ui.PaintConstString(17, 20, "║");
    Ui.PaintConstString(39, 20, "║");
    Ui.PaintConstString(51, 20, "║");
    Ui.PaintConstString(59, 20, "║");
    Ui.PaintConstString(69, 20, "║");
    Ui.PaintConstString(85, 20, "║");
    Ui.PaintConstString(91, 20, "║");
    Ui.PaintConstString(97, 20, "║");
    Ui.PaintConstString(11, 21, "╟═════╩═════════════════════╩═══════════╩═══════╩═════════╩═══════════════╩═════╩═════╝");
    Ui.PaintConstString(11, 22, "╟═════════╗");
    Ui.PaintConstString(12, 23, " 选课情况║");
    Ui.PaintConstString(11, 24, "╟═════════════════════════════════════════════════════════════════════════════════════╗");
}
void CreatRootUI_Son_EditClass()
{
    for (int i = 0; i < 15; i++)
    {
        Ui.PaintConstString(13, i + 22, "                                                                                     ");
    }
    Ui.PaintConstString(11, 24, "╟═════╦═════════════════════╦═══════════╦═══════╦═════════╦═══════════════╦═════╦═════╗");
    Ui.PaintConstString(13, 25, "编号║         名称        ║    性质   ║ 总学时║ 授课学时║ 实验或上机学时║ 学分║ 容量║");
    Ui.PaintConstString(11, 26, "╟═════╬═════════════════════╬═══════════╬═══════╬═════════╬═══════════════╬═════╬═════╢");
    Ui.PaintConstString(17, 27, "║");
    Ui.PaintConstString(39, 27, "║");
    Ui.PaintConstString(51, 27, "║");
    Ui.PaintConstString(59, 27, "║");
    Ui.PaintConstString(69, 27, "║");
    Ui.PaintConstString(85, 27, "║");
    Ui.PaintConstString(91, 27, "║");
    Ui.PaintConstString(97, 27, "║");
    Ui.PaintConstString(11, 28, "╟═════╩═════════════════════╩═══════════╩═══════╩═════════╩═══════════════╩═════╩═════╢");
    Ui.PaintConstString(11, 22, "╟═════════════════════════════════════════════════════════════════════════════════════╢");
    Ui.PaintConstString(12, 23, " 改：");
    Ui.PaintConstString(13, 29, "内容：");
}

