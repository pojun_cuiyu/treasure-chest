移植代码注意事项：
文件目录：


1、首先注意vscode编译环境
	需要在同级文件夹.vscode里添加.launch文件和.tasks文件
	配置修改
	（1）task：
		type:-->shell
		label:-->task g++
		command和cwd对应到vscode的系统文件
		在"${file}",和"-o",之间添加功能文件的名字（该文件需要在同级文件下）
	（2）launch
		cwd:-->"${workspaceFolder}"
		miDebuggerPath对应到vscode的系统文件
		preLaunchTask对应task文件的label-->task g++

2、文件的错误修改
	（1）studClass：
		在使用MessageBox函数的第二个和第三个变量的双引号外添加TEXT()包裹
		将gets函数中第二个变量删除
		删除多余的struct。因为自定义命名的结构体在定义结构变量时不需要多余的struct
	（2）在"${file}",和"-o",之间添加功能文件的名字
		这一步为了填补VS2019与VSCODE的系统文件的配置缺陷
		"${fileDirname}\\CreatUI.cpp",
        "${fileDirname}\\base.cpp",
        "${fileDirname}\\InputORoutputdata.cpp",
        "${fileDirname}\\check.cpp",
	填补完了，会发现base/InputORoutputdata/check文件都存在微小的报错
	因为C语言的系统文件在VSCODE2019的版本经过修改，在VSCODE中存在报错
	（3）base：
		将char ary[ ]修改为char* ary。
	（4）InputORoutputdata：
		删除erro_t error
		将error改为fp
		将fopen_s改为fopen
		删除“fp”
	（5）check：
		删除erro_t error
		将error改为fp
		将fopen_s改为fopen
		删除第一个变量fp

3、将文件夹中的中文字体删除

4、消除中文乱码（前提是文件在UTF-8的编译下没有中文乱码）
	在左下角，点击UTF-8选择GB2312保存，重新调试便可
	如果在乱码的状态下进行保存将无法回复
	