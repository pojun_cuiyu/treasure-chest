#include "check.h"

student studentlist[7] = {};
extern student StudentNewNote; // 全局变量，当前学生的信息

// 登录的账号密码检测,登录检测用的还是之前的文件系统，懒得改了
int CheckPword()
{
    FILE *fp;
    char ch;                 // 字符指针
    int usernb = 0;          // 用户数
    char username[32] = {};  // 用户账号指针
    char userpword[32] = {}; // 用户密码指针
    // char realname[32] = {};
    // int classlist[6] = {};
    int i = 0;
    fp = fopen("UserData.txt", "r");
    if (fp == NULL) // 如果文件不存在
    {
        return 1;
    }
    // fgetc（）位置指针先自动下移，并读取该地址的一个字符，
    while ((ch = fgetc(fp)) != EOF) //////统计用户数    eof文件结束符
    {
        if (ch == '#')
        {
            usernb++;
        }
    }
    rewind(fp); // 将文件的位置指针重新指向开头

    for (int j = 0; j < usernb; j++) // 逐个用户
    {
        i = 0;
        ch = fgetc(fp);
        while (ch != ';') // 用username逐个字符获取用户账号
        {
            username[i] = ch;
            i++;
            ch = fgetc(fp);
        }
        if (strcmp(StudentNewNote.name, username) == 0) ////如果账号有相同
        {
            i = 0;
            ch = fgetc(fp);
            while (ch != ';') // 用userpword逐个字符获取用户密码
            {
                userpword[i] = ch;
                i++;
                ch = fgetc(fp);
            }
            if (strcmp(StudentNewNote.pword, userpword) == 0) // 登录成功,将用户信息传入
            {
                ReadF_ary(fp, ch, StudentNewNote.realname); // 将用户名赋值
                fclose(fp);
                return 1;
            }
        }
        while (ch != '\n') // 过度该用户的其他信息
        {
            ch = fgetc(fp);
        }
    }
    fclose(fp); // 关闭文件
    return 0;
}

// 检测课程是否被当前学生选择，注意学生所选课程的数组容量为6
int CheckExistClass(int i, SchoolClass SchoolClassList[], student StudentNewNote)
{
    for (int j = 0; j < 6; j++)
    {
        if (StudentNewNote.classlist[j] == SchoolClassList[i].number)
        {
            return 1;
        }
    }
    return 0;
}