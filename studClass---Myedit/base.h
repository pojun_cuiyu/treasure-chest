#ifndef base_H
#define base_H

#include <iostream>
#include <windows.h>
#include <conio.h>
#include <string.h>
using namespace std;

class Cursor
{
private:
    void *handle; // 页面句柄
    COORD coord;  // 光标位置
public:
    Cursor();
    void PaintConstString(short x, short y, const char *string);
    void PaintDynaString(short x, short y, char *string);
    void PaintNumber(short x, short y, int number);
    void Relocate(short x, short y);
    void OriginOutput(char *string);
} Ui;

Cursor::Cursor()
{
    system("mode con cols=100 lines=40"); //////设定窗口大小
    handle = GetStdHandle(STD_OUTPUT_HANDLE);
}

int LogInputing(char ARY[32], int &notelong);
int ReadF_atoi(char txt[1024], int &index);            // 缓存数组中字符串转换为整型的函数
void Read_ary(char txt[1024], int &index, char ary[]); // 读取缓存数组中字符串的函数
void ReadF_ary(FILE *fp, char &ch, char ary[]);        // 给密码检测用的
char GetAChar(char txt[1024], int &index);
typedef struct InputingState ////////输入功能的状态机
{
    char txt[32]; // 内容
    int txtlong;  // 长度变量
} InputingState;

typedef struct SchoolClass // 课程信息
{
    int number;      // 编号
    char name[32];   // 名称
    char nature[32]; // 名称
    int Allhours;    // 总学时
    int roomhours;   // 授课学时
    int esphours;    // 特殊学时
    int credit;      // 学分
    int capacity;    // 课程容量
    char note[64];   // 课程内容简介
} SchoolClass;
typedef struct student // 学生信息
{
    char realname[32]; // 名字
    char name[32];     // 账户
    char pword[32];    // 密码
    int classlist[6];  // 选课的列表
    int sumofclass;    // 选课总学分
    int sumofcredit;   // 选课的门数
} student1;

#endif
