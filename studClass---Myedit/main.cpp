#include "base.cpp"
#include "CreatUI.cpp"
#include "check.cpp"
#include "inputORoutputdata.cpp"
#include "control.cpp"
/*
 * pagelist
 * page=1:登录界面
 * page=2:普通用户界面
 * page=3:管理员界面
 * page=4:普通用户选课界面
 * page=5:管理员管理课程界面
 * page=6:普通用户我的课程界面
 * page=7:管理员所有课程界面
 * page=8:管理员修改课程界面
 */

int page = 1; // 当前的界面，初始化为登录界面
int main()
{
    system("mode con cols=100 lines=40"); //////设定窗口大小
    MessageBox(0, TEXT("请每名同学至少选2门课,最多选3门课,且选课总学分不超过10分。\n管理员请用root账号登录。"), TEXT("通知，五邑小学夏季选课现已开始。"), 0);
    Usernote user = {{}, {}};
    // 初始化学生的选课列表，否则运行失败
    for (int i = 0; i < 6; i++)
    {
        StudentNewNote.classlist[i] = -1;
    }
    hout = GetStdHandle(STD_OUTPUT_HANDLE);
    UserDataInit(Usertxt);
    ClassDataInit(Classtxt);
    while (1)
    {
        system("cls");
        system("mode con cols=100 lines=40"); //////设定窗口大小
        CreatStartUI();                       // 页面制表符输出
        StartUI();                            // 页面交互元素和键盘控制函数
        while (page == 3)                     //////跳转到管理员界面
        {
            system("cls");
            CreatRootUI();
            RootUI();
            while (page == 7) //////跳转到管理员界面_子界面_所有课程
            {
                system("cls");
                CreatRootUI();
                CreatRootUI_AllClass();
                Root_AllClassUI();
                SaveData("ClassData.txt", Classtxt);
            }
            while (page == 5) //////跳转到用户界面_子界面_管理课
            {
                system("cls");
                CreatRootUI();
                CreatRootUI_ManageClass();
                Root_ManageClassUI();
                SaveData("ClassData.txt", Classtxt);
            }
        }
        while (page == 2) //////跳转到用户界面
        {
            system("cls");

            CreatUserUI();
            UserUI();

            while (page == 4) //////跳转到用户界面_子界面_选课
            {
                system("cls");
                CreatUserUI();
                CreatUserUI_Class();
                User_SeleClassUI();
                SaveData("UserData.txt", Usertxt);
            }
            while (page == 6) //////跳转到用户界面_子界面_我的课
            {
                system("cls");
                CreatUserUI();
                CreatUserUI_Class();
                User_MyClassUI();
                SaveData("UserData.txt", Usertxt);
            }
        }
    }
    return 0;
}
