#include "base.h"

char GetAChar(char txt[1024], int &index);
typedef struct UIele
{
    const char *name; // 元素的名字
    short X;          // 光标位置
    short Y;
    int notelong; // 输入内容的长度
} UIele;

void Cursor::PaintConstString(short x, short y, const char *string)
{
    coord.X = x;
    coord.Y = y;
    SetConsoleCursorPosition(handle, coord);
    cout << string; // X=50
}
// 绘画动态字符串
void Cursor::PaintDynaString(short x, short y, char *string)
{
    coord.X = x;
    coord.Y = y;
    SetConsoleCursorPosition(handle, coord);
    cout << string; // X=50
}
// 绘制数字
void Cursor::PaintNumber(short x, short y, int number)
{
    coord.X = x;
    coord.Y = y;
    SetConsoleCursorPosition(handle, coord);
    printf("%d", number); // X=50
}
void Cursor::Relocate(short x, short y)
{
    coord.X = x;
    coord.Y = y;
    SetConsoleCursorPosition(handle, coord);
}

void Cursor::OriginOutput(char *string)
{
    cout << string; // X=50
}

// 转换读取函数
int ReadF_atoi(char txt[1024], int &index)
{
    char ch;
    char temp[2] = {};
    for (int i = 0; i < 2; i++) // 读两位数字
    {
        ch = GetAChar(txt, index);
        temp[i] = ch;
    }
    ch = GetAChar(txt, index);
    return atoi(temp);
}
/*
 * 参量：文件指针、字符指针、存储数组
 * 描述：
 */
void Read_ary(char txt[1024], int &index, char *ary)
{
    char ch;
    for (int j = sizeof(ary); j > 0; j--) // 从高位开始清空存储数组
    {
        ary[j] = '\0';
    }
    int i = 0;
    ch = GetAChar(txt, index);
    while (ch != ';')
    {
        if (ch != 32 && ch != '$') //空格符和$符号是占位符，不输入
        {
            ary[i] = ch;
            i++;
        }
        ch = GetAChar(txt, index);
    }
    ary[i] = '\0';
}
/*
 * 参量：文件指针、字符指针
 * 描述：取值下移，类比 fgetc()
 */
char GetAChar(char txt[1024], int &index) // 先取值，后右移
{
    char ch;
    ch = txt[index];
    index++;
    return ch;
}
void ReadF_ary(FILE *fp, char &ch, char *ary)
{
    for (int j = sizeof(ary); j > 0; j--)
    {
        ary[j] = '\0';
    }
    int i = 0;
    ch = fgetc(fp);
    while (ch != ';')
    {
        if (ch != 32) // 不是空格
        {
            ary[i] = ch;
            i++;
        }
        ch = fgetc(fp);
    }
}
