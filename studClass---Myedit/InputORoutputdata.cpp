#include "InputORoutputdata.h"

/*
typedef struct SchoolClass
{
    int number;//编号
    char name[32];//名称
    char nature[32];//性质
    int Allhours;//总学时
    int roomhours;//授课学时
    int esphours;//特殊学时
    int credit;//学分
    int capacity;//课程容量
    char note[64];//课程内容简介
}SchoolClass;
*/
extern char Usertxt[1024];
extern char Classtxt[1024];
extern student StudentNewNote;
//读取课程数据
extern SchoolClass SchoolClassList[7];
int ReadClassData(char Classtxt[1024])
{
    
    char ch;
    int classnb = 0;//课程数，小于八
    char temp[16] = {};
    int i = 0;
    int j = 0;
    int index = 0;
    ch=GetAChar(Classtxt, index);
    while (ch  != '\0')
    {
        if (ch == '#')
        {
            classnb++;
        }
        ch = GetAChar(Classtxt, index);
    }
    //限制为7个课程
    if (classnb > 7)
    {
        classnb = 7;
    }
    index = 0;
    //开始读取课程
    for (int i = 0; i < classnb; i++)
    {
        SchoolClassList[i].number = ReadF_atoi(Classtxt, index);
        Read_ary(Classtxt,index, SchoolClassList[i].name);
        Read_ary(Classtxt, index, SchoolClassList[i].nature);
        SchoolClassList[i].Allhours = ReadF_atoi(Classtxt, index);
        SchoolClassList[i].roomhours = ReadF_atoi(Classtxt, index);
        SchoolClassList[i].esphours = ReadF_atoi(Classtxt, index);
        SchoolClassList[i].credit = ReadF_atoi(Classtxt, index);
        SchoolClassList[i].capacity = ReadF_atoi(Classtxt, index);
        Read_ary(Classtxt, index, SchoolClassList[i].note);
        ch = Classtxt[index];
        while (ch != '\n' && ch != '\0')
        {
            ch = GetAChar(Classtxt, index);
        }
    }
    return 1;
}
int AddUserClass(char* name, int classnumber, char Usertxt[1024],int credit)
{
    char ch;
    int index = 0;
    int i = 0;
    char temp[16] = {};
    int usernb = 0;
    ch = GetAChar(Usertxt, index);
    while (ch != '\0')//////统计用户数
    {
        if (ch == '#')
        {
            usernb++;
        }
        ch = GetAChar(Usertxt, index);
    }
    index = 0;
    for (int j = 0; j < usernb; j++)
    {
        ch = GetAChar(Usertxt, index);
        while (ch != ';')
        {
            temp[i] = ch;
            i++;
            ch = GetAChar(Usertxt, index);
        }
        temp[i] = '\0';
        i = 0;
        if (strcmp(temp,name) == 0)//找到用户信息所在行
        {
            for (int i = 0; i < 2; i++)//跨过一定个数的信息块
            {
                ch = GetAChar(Usertxt, index);
                while (ch != ';')
                {
                    ch = GetAChar(Usertxt, index);
                }
            }
            ch = GetAChar(Usertxt, index);
            int addover = 0;
            while (ch != ';')//添加所选课程
            {
                if (ch == '0'&& addover == 0)
                {
                    int wei10 = index - 1;
                    ch = GetAChar(Usertxt, index); 
                    if (ch == '0')
                    {
                        int wei1 = index - 1;
                        Usertxt[wei10] = (classnumber / 10)+48;
                        Usertxt[wei1] = (classnumber % 10)+48;
                        addover = 1;
                    }
                }
                else
                ch = GetAChar(Usertxt, index);
            }
            StudentNewNote.sumofclass++;//用户所选的课程数+1
            ch = GetAChar(Usertxt, index);
            Usertxt[index-1] = (StudentNewNote.sumofclass / 10)+48;
            ch = GetAChar(Usertxt, index);
            Usertxt[index - 1] = (StudentNewNote.sumofclass % 10) + 48;
            StudentNewNote.sumofcredit += credit;//用户所选的学分更新
            ch = GetAChar(Usertxt, index);//重复的GetAChar是为了跳过分号
            ch = GetAChar(Usertxt, index);
            Usertxt[index - 1] = (StudentNewNote.sumofcredit / 10) + 48;
            ch = GetAChar(Usertxt, index);
            Usertxt[index - 1] = (StudentNewNote.sumofcredit % 10) + 48;
            return 1;
        }
        while (ch != '\n' && ch != '\0')
        {
            ch = GetAChar(Usertxt, index);
        }
        for (int i = 0; temp[i] != '\0'; i++)
        {
            temp[i] = '\0';
        }
    }
    return 0;
}
int DeleteUserClass(char* name, int classnumber, char Usertxt[1024],int credit)
{
    char ch;
    int index = 0;
    int i = 0;
    char temp[16] = {};
    int usernb = 0;
    ch = GetAChar(Usertxt, index);
    while (ch != '\0')//////统计用户数
    {
        if (ch == '#')
        {
            usernb++;
        }
        ch = GetAChar(Usertxt, index);
    }
    index = 0;
    for (int j = 0; j < usernb; j++)
    {
        ch = GetAChar(Usertxt, index);
        while (ch != ';')
        {
            temp[i] = ch;
            i++;
            ch = GetAChar(Usertxt, index);
        }
        temp[i] = '\0';
        i = 0;
        if (strcmp(temp, name) == 0)//找到用户信息所在行
        {
            for (int i = 0; i < 2; i++)//跨过一定个数的信息块
            {
                ch = GetAChar(Usertxt, index);
                while (ch != ';')
                {
                    ch = GetAChar(Usertxt, index);
                }
            }
            ch = GetAChar(Usertxt, index);
            while (ch != ';')//删除所选课程
            {
                if (ch == (classnumber/10)+48)
                {
                    int wei10 = index - 1;
                    ch = GetAChar(Usertxt, index);
                    if (ch == (classnumber % 10) + 48)
                    {
                        int wei1 = index - 1;
                        Usertxt[wei10] = '0';
                        Usertxt[wei1] = '0';
                    }
                }
                else
                ch = GetAChar(Usertxt, index);
            }
            StudentNewNote.sumofclass--;
            ch = GetAChar(Usertxt, index);
            Usertxt[index - 1] = (StudentNewNote.sumofclass / 10) + 48;
            ch = GetAChar(Usertxt, index);
            Usertxt[index - 1] = (StudentNewNote.sumofclass % 10) + 48;
            StudentNewNote.sumofcredit -= credit;
            ch = GetAChar(Usertxt, index);//重复的GetAChar是为了跳过分号
            ch = GetAChar(Usertxt, index);
            Usertxt[index - 1] = (StudentNewNote.sumofcredit / 10) + 48;
            ch = GetAChar(Usertxt, index);
            Usertxt[index - 1] = (StudentNewNote.sumofcredit % 10) + 48;

            return 1;
        }
        while (ch != '\n' && ch != '\0')
        {
            ch = GetAChar(Usertxt, index);
        }
        for (int i = 0; temp[i] != '\0'; i++)
        {
            temp[i] = '\0';
        }
    }
    return 0;
}
int GetUserClass(char Usertxt[1024])//传入当前用户的账号
{

    char ch;
    int index = 0;
    int i = 0;
    char temp[16] = {};
    int usernb = 0;
    ch=GetAChar(Usertxt, index);
    while (ch!='\0')//////统计用户数
    {
        if (ch == '#')
        {
            usernb++;
        }
        ch = GetAChar(Usertxt, index);
    }
    index = 0;
    for (int j = 0; j < usernb; j++)
    {
        ch = GetAChar(Usertxt, index);
        while (ch != ';')
        {
            temp[i] = ch;
            i++;
            ch = GetAChar(Usertxt, index);
        }
        temp[i] = '\0';
        i = 0;
        if (strcmp(temp, StudentNewNote.name) == 0)//找到用户信息所在行
        {
            for (int i = 0; i < 2; i++)//跨过一定个数的信息块
            {
                ch = GetAChar(Usertxt, index);
                while (ch != ';')
                {
                    ch = GetAChar(Usertxt, index);
                }
            }
            ch = GetAChar(Usertxt, index);
            int k = 0;
            while (ch != ';')//传入选课的序号
            {
                if (ch == '+') 
                { 
                    ch = GetAChar(Usertxt, index);
                    continue;
                }
                else
                {
                        char wei10 = ch;
                        ch = GetAChar(Usertxt, index);
                        char wei1 = ch;
                        if ((wei10 != '0'|| wei1 != '0')&& wei1!='+'&& wei1 != ';')//这三个条件分别是：1.序号不能是00(课程不存在)2.避免混入错误的+号3.边际的检测
                        {
                            StudentNewNote.classlist[k] = (wei10 - 48) * 10 + (wei1 - 48);
                            k++;
                        }
                }
            }
            ch = GetAChar(Usertxt, index);
            char sumofclass[2] = {};
            for (int i = 0; i < 2; i++)//传入选课的门数
            {
                ch = GetAChar(Usertxt, index);
                sumofclass[i] = ch;
            }
            StudentNewNote.sumofclass=atoi(sumofclass);
            char sumofcredit[2] = {};
            for (int i = 0; i < 2; i++)//传入选课的学分
            {
                ch = GetAChar(Usertxt, index);
                sumofcredit[i] = ch;
            }
            StudentNewNote.sumofcredit = atoi(sumofcredit);
            return 1;
        }
        while (ch != '\n'&& ch != '\0')
        {
            ch = GetAChar(Usertxt, index);
        }
        for (int i = 0; temp[i] != '\0'; i++)
        {
            temp[i] = '\0';
        }
    }
    return 0;
}
//用户数据初始化
int UserDataInit(char Usertxt[1024])
{
    FILE* fp;
    char ch;
    unsigned int i = 0;
    int usernb = 0;
    //////打开用户数据文件
    fp = fopen("UserData.txt", "r+");
    if (fp == NULL)
    {
        return 1;
    }//打开失败返回1
    ch = fgetc(fp);//读取文件内的第一个字符
    while (ch != EOF)//如果不是文件结尾，将持续进行
    {   ////获取文件中出个空格以外的所有字符
        if (ch != 32)//32对应"$"空格
        {
            Usertxt[i] = ch;
            i++;
        }
        ch = fgetc(fp);//读取文件的下一个字符
    }
    fclose(fp);//关闭文件
    return 0;
}
//课程数据初始化
int ClassDataInit(char Classtxt[1024])
{
    FILE* fp;
    char ch;
    unsigned int i = 0;
    int usernb = 0;
    fp = fopen("ClassData.txt", "r+");
    if (fp == NULL)
    {
        return 1;
    }
    ch = fgetc(fp);
    while (ch != EOF)
    {
        if (ch != 32)
        {

            Classtxt[i] = ch;
            i++;
        }
        ch = fgetc(fp);
    }
    fclose(fp);
    return 0;
}
//读取某课的学生，参数是课的序号，缓存，存储学生的列表
int ReadClassSelecter(int number, char Usertxt[1024], char ClassSelecter[16][16])
{
    char ch;
    int index = 0;
    int i = 0;
    char TClassSelecter[16][16] = {};
    int usernb = 0;
    ch = GetAChar(Usertxt, index);
    while (ch != '\0')//////统计用户数
    {
        if (ch == '#')
        {
            usernb++;
        }
        ch = GetAChar(Usertxt, index);
    }
    index = 0;
    for (int j = 0; j < usernb; j++)
    {

        for (int i = 0; i < 2; i++)//跨过一定个数的信息块
        {
            ch = GetAChar(Usertxt, index);
            while (ch != ';')
            {
                ch = GetAChar(Usertxt, index);
            }
        }
        ch = GetAChar(Usertxt, index);
        while (ch != ';')
        {
            TClassSelecter[j][i] = ch;
            i++;
            ch = GetAChar(Usertxt, index);
        }
        TClassSelecter[j][i] = '\0';
        i = 0;
        ch = GetAChar(Usertxt, index);
        while (ch != ';')
        {
            if (ch == '+')
            {
                ch = GetAChar(Usertxt, index);
                continue;
            }
            else
            {
                char wei10 = ch;
                ch = GetAChar(Usertxt, index);
                char wei1 = ch;
                if ((wei10 != '0' || wei1 != '0') && wei1 != '+' && wei1 != ';')//这三个条件分别是：1.序号不能是00(课程不存在)2.避免混入错误的+号3.边际的检测
                {
                    if (number == (wei10 - 48) * 10 + (wei1 - 48))
                    {
                        for (int i = 0; TClassSelecter[j][i] != '\0'; i++)
                        {
                            ClassSelecter[j][i] = TClassSelecter[j][i];
                        }
                        break;
                    }

                }
            }
        }
        while (ch != '\n' && ch != '\0')
        {
            ch = GetAChar(Usertxt, index);
        }

    }
    return 0;
}
//listnb是需要修改的课程信息在课程结构体中的位置,EditNote是替换的课程信息
int EditClass(int classnb, int listnb,char Classtxt[1024],char EditNote[])//listnb是需要修改的课程信息在课程结构体中的位置
{
    char ch;
    int index = 0;
    int i = 0;
    char temp[16] = {};
    int usernb = 0;
    ch = GetAChar(Classtxt, index);
    while (ch != '\0')//////统计用户数
    {
        if (ch == '#')
        {
            usernb++;
        }
        ch = GetAChar(Classtxt, index);
    }
    index = 0;
    for (int j = 0; j < usernb; j++)
    {
        ch = GetAChar(Classtxt, index);
        while (ch != ';')
        {
            temp[i] = ch;
            i++;
            ch = GetAChar(Classtxt, index);
        }
        temp[i] = '\0';
        i = 0;
        if (classnb == atoi(temp))//找到课程信息所在行
        {
            if (listnb == 0)
            { 
                return 0;
            }
            for (int i = 0; i < listnb-1; i++)//跨过一定个数的信息块
            {
                ch = GetAChar(Classtxt, index);
                while (ch != ';')
                {
                    ch = GetAChar(Classtxt, index);
                }
            }
         int Editindex = index;
         ch = GetAChar(Classtxt, index);
         while (ch != '$'&& ch != ';')
         {
             Classtxt[index-1] = '$';
             ch = GetAChar(Classtxt, index);
         }
         index = Editindex;
         for (int i = 0; EditNote[i] != '\0'; i++)
         {
             Classtxt[index++] = EditNote[i];
         }
            return 1;
        }
        while (ch != '\n' && ch != '\0')
        {
            ch = GetAChar(Classtxt, index);
        }
        for (int i = 0; temp[i] != '\0'; i++)
        {
            temp[i] = '\0';
        }
    }
    return 0;
}
int SaveData(const char* name, char  txt[1024])
{

    FILE* fp;
    int error;
    char ch;
    int index = 0;
    fp = fopen(name, "w");
    if (fp == NULL)
    {
        return 1;
    }
    ch = GetAChar(txt, index);
    while (ch !='\0')//////统计用户数
    {
        if (ch != 32)
        {
            fputc(ch, fp);
            
        }
        ch = GetAChar(txt, index);
    }
    fclose(fp);
    return 0;
}