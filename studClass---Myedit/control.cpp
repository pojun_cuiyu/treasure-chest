#include "control.h"
void bouble(struct SchoolClass array[], int n);
int Root_EditClassUI(int nbinlist);
void EmptyClassNote();
extern int page;
extern SchoolClass SchoolClass1;
extern SchoolClass SchoolClassList[7];

int OutThisPage = 0;
char ClassSelecter[16][16] = {}; // 用在root界面显示某课程的所有学生的数组
typedef struct Usernote          // 存储登录信息
{
    char name[NAME_L];
    char pword[PWORD_L];
    // 匹配账号密码对应用户数据的前两个存储单元
} Usernote;

struct student StudentNewNote; // 学生数据的中间结构变量
SchoolClass SchoolClassList[7] = {};
char Usertxt[1024];
// 单个用户数据格式：账号；密码；用户名；选课列表；总学分；总学时；#
char Classtxt[1024];
// 单个课程数据格式：课程序号；课程名称（十六个空格）；课程性质（八个空格）；总学时；授课学时；实验或上机学时；学分；学生容量；课程介绍（字符上限40，多余的空格补齐）；#
int ch;                          // 获取输入键码字符的中间
HANDLE hout;                     // 窗口大小：40x100
COORD coord;                     // 屏幕上光标的坐标
CONSOLE_SCREEN_BUFFER_INFO csbi; // 控制台屏幕缓冲区信息

// 开始页面交互元素和键盘控制函数
void StartUI()
{ // 交互元素的结构体变量
    typedef struct UIele
    {
        const char *name; // 元素的名字
        short X;          // 光标位置
        short Y;
        int notelong; // 输入内容的长度

    } UIele;
    UIele name = {"账号", 42, 15, 0};
    UIele pword = {"密码", 42, 18, 0};
    UIele logon = {"登录", 45, 21};
    UIele UIelementST[3][1] = {name, pword, logon};
    // 三个可交互元素的列表，及其初始的横坐标，方便后续的字符输入
    int UIeleSelect = 0;
    int AutoNextorNot = 0; // 输入完后是否自动换下一个元素
    // 光标的初始位置吸附账号元素的位置
    coord.X = UIelementST[UIeleSelect][0].X;
    coord.Y = UIelementST[UIeleSelect][0].Y;
    SetConsoleCursorPosition(hout, coord); // 设置光标
    // 指导用户使用的信息指引
    Ui.PaintConstString(0, 39, "请回车输入账号密码");
    // 0x0d表示回车，0XE0表示上下左右等键的键码
    while (page == 1)
    {
        ch = _getch();
        if (ch == 0x48) // 上
        {
            UIeleSelect--;
            if (UIeleSelect <= -1)
            {
                UIeleSelect = 0; // 下限归零
            }
            coord.X = UIelementST[UIeleSelect][0].X;
            coord.Y = UIelementST[UIeleSelect][0].Y;
            SetConsoleCursorPosition(hout, coord);
        }
        if (ch == 0x50) // 下
        {
            UIeleSelect++;
            if (UIeleSelect >= 3)
            {
                UIeleSelect = 2; // 上限置顶
            }
            coord.X = UIelementST[UIeleSelect][0].X;
            coord.Y = UIelementST[UIeleSelect][0].Y;
            SetConsoleCursorPosition(hout, coord);
        }
        // 如果在选中元素时回车
        if (ch == 0x0d)
        {

            if (UIelementST[UIeleSelect][0].name == "登录")
            {
                if (CheckPword() == 1)
                {
                    Ui.PaintConstString(0, 39, "登录成功            ");
                    if (strcmp(StudentNewNote.name, "root") == 0)
                    {
                        page = 3; // 跳转到管理界面
                    }
                    else
                    {
                        page = 2; // 跳转到用户界面
                    }
                }
                else
                {
                    Ui.PaintConstString(0, 39, "登录失败            ");

                    coord.X = UIelementST[2][0].X;
                    coord.Y = UIelementST[2][0].Y;
                    SetConsoleCursorPosition(hout, coord); // 如果登陆失败，光标回到登录
                }
            }
            if (UIelementST[UIeleSelect][0].name == "账号")
            {
                Ui.PaintConstString(0, 39, "正在输入账号               ");
                coord.X = UIelementST[UIeleSelect][0].X;
                coord.Y = UIelementST[UIeleSelect][0].Y;
                SetConsoleCursorPosition(hout, coord);
                gets(&StudentNewNote.name[0]);

                UIeleSelect++;
                if (UIeleSelect >= 3)
                {
                    UIeleSelect = 2;
                }
                Ui.PaintConstString(0, 39, "请回车输入账号密码");
                coord.X = UIelementST[UIeleSelect][0].X;
                coord.Y = UIelementST[UIeleSelect][0].Y;
                SetConsoleCursorPosition(hout, coord);
            }
            if (UIelementST[UIeleSelect][0].name == "密码")
            {
                Ui.PaintConstString(0, 39, "正在输入密码                 ");
                coord.X = UIelementST[UIeleSelect][0].X;
                coord.Y = UIelementST[UIeleSelect][0].Y;
                SetConsoleCursorPosition(hout, coord);
                gets(&StudentNewNote.pword[0]);
                UIeleSelect++;
                if (UIeleSelect >= 3)
                {
                    UIeleSelect = 2;
                }
                Ui.PaintConstString(0, 39, "请回车输入账号密码      ");
                coord.X = UIelementST[UIeleSelect][0].X;
                coord.Y = UIelementST[UIeleSelect][0].Y;
                SetConsoleCursorPosition(hout, coord);
            }
        }
    }
}
void UserUI()
{
    typedef struct UIele
    {
        const char *name; // 元素的名字
        short X;          // 光标位置
        short Y;
    } UIele;
    UIele SeleClass = {"选课", 2, 7};
    UIele MyClass = {"我的课程", 0, 13};
    UIele OutLogon = {"退出登录", 73, 2};
    UIele OutSys = {"退出系统", 88, 2};
    UIele UIelement[2][3] = {{SeleClass, OutLogon, OutSys}, MyClass}; // 可交互元素的列表，分别是账号，密码，登录
    static int UIeleSelect_X = 0;
    static int UIeleSelect_Y = 0;
    coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
    coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
    Ui.PaintDynaString(13, 1, StudentNewNote.realname); ////////用真名欢迎
    Ui.PaintConstString(0, 39, "                                        ");
    SetConsoleCursorPosition(hout, coord); // 设置光标的初始位置在选课上                             //0x0d表示回车，0XE0表示上下左右等键的键码
    while (page == 2)
    {
        ch = _getch();
        // 光标向其上元素移动
        if (ch == 0x48)
        {
            if (UIelement[UIeleSelect_Y][UIeleSelect_X].name == "选课")
            {
                UIeleSelect_X = 1;
            }
            UIeleSelect_Y--;
            if (UIeleSelect_Y <= -1)
            {
                UIeleSelect_Y = 0;
            }
            coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
            coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
            SetConsoleCursorPosition(hout, coord);
        }
        // 光标向其下元素移动
        if (ch == 0x50)
        {
            if (UIelement[UIeleSelect_Y][UIeleSelect_X].name == "退出登录" || UIelement[UIeleSelect_Y][UIeleSelect_X].name == "退出系统") // 自动换行
            {
                UIeleSelect_X = 0;
                UIeleSelect_Y = 0;
            }
            else
            {
                UIeleSelect_Y++;
                if (UIeleSelect_Y >= 2)
                {
                    UIeleSelect_Y = 1;
                }
            }
            coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
            coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
            SetConsoleCursorPosition(hout, coord);
        }
        // 光标向其左元素移动
        if (ch == 0x4b)
        {
            UIeleSelect_X--;
            if (UIeleSelect_X <= -1)
            {
                UIeleSelect_X = 0;
            }
            coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
            coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
            SetConsoleCursorPosition(hout, coord);
        }
        // 光标向其右元素移动
        if (ch == 0x4d)
        {
            if (UIelement[UIeleSelect_Y][UIeleSelect_X].name == "我的课程") // 自动换行
            {
                UIeleSelect_Y = 0;
                UIeleSelect_X = 1;
            }
            else
            {
                UIeleSelect_X++;
                if (UIeleSelect_X >= 2)
                {
                    UIeleSelect_X = 2;
                }
            }
            coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
            coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
            SetConsoleCursorPosition(hout, coord);
        }
        // 如果在选中元素时回车
        if (ch == 0x0d)
        {
            if (UIelement[UIeleSelect_Y][UIeleSelect_X].name == "选课")
            {
                page = 4;
            }
            if (UIelement[UIeleSelect_Y][UIeleSelect_X].name == "我的课程")
            {
                page = 6;
            }
            if (UIelement[UIeleSelect_Y][UIeleSelect_X].name == "退出系统")
            {
                Ui.PaintConstString(0, 39, "(任意键取消)再按一次回车键退出系统");
                ch = _getch();
                if (ch != 0x0d)
                {
                    Ui.PaintConstString(0, 39, "                                           ");
                }
                else
                {
                    system("cls");
                    exit(1);
                }
            }
            if (UIelement[UIeleSelect_Y][UIeleSelect_X].name == "退出登录")
            {
                Ui.PaintConstString(0, 39, "(任意键取消)再按一次回车键退出登录");
                ch = _getch();
                if (ch != 0x0d)
                {
                    Ui.PaintConstString(0, 39, "                                           ");
                }
                else
                {
                    for (int i = 0; i < 32; i++)
                    {
                        StudentNewNote.name[i] = '\0';
                        StudentNewNote.pword[i] = '\0';
                        // 将数据清零
                    }
                    page = 1;
                }
            }
        }
    }
}
void RootUI()
{
    typedef struct UIele
    {
        const char *name; // 元素的名字
        short X;          // 光标位置
        short Y;
    } UIele;
    UIele AllClass = {"所有课程", 0, 7};
    UIele ManageClass = {"管理课程", 0, 13};
    UIele ClassST = {"选课情况", 0, 19};
    UIele OutLogon = {"退出登录", 73, 2};
    UIele OutSys = {"退出系统", 88, 2};
    UIele UIelement[2][3] = {{AllClass, OutLogon, OutSys}, {ManageClass}};
    // 可交互元素的列表，
    static int UIeleSelect_X = 0;
    static int UIeleSelect_Y = 0;
    coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
    coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
    Ui.PaintDynaString(13, 1, StudentNewNote.realname); ////////用真名欢迎
    Ui.PaintConstString(0, 39, "                                      ");
    SetConsoleCursorPosition(hout, coord); // 设置光标的初始位置在课程上
    while (page == 3)
    {
        ch = _getch();
        // 光标向其上元素移动
        if (ch == 0x48)
        {
            if (UIelement[UIeleSelect_Y][UIeleSelect_X].name == "所有课程")
            {
                UIeleSelect_X = 1;
            }
            UIeleSelect_Y--; // 如果光标在管理课程上，则跳到所有课程
            if (UIeleSelect_Y <= -1)
            {
                UIeleSelect_Y = 0;
            }
            coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
            coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
            SetConsoleCursorPosition(hout, coord);
        }
        // 光标向其下元素移动
        if (ch == 0x50)
        {
            if (UIelement[UIeleSelect_Y][UIeleSelect_X].name == "退出登录" || UIelement[UIeleSelect_Y][UIeleSelect_X].name == "退出系统") // 自动换行
            {
                UIeleSelect_X = 0;
                UIeleSelect_Y = 0;
            }
            else
            {
                UIeleSelect_Y++;
                if (UIeleSelect_Y >= 2)
                {
                    UIeleSelect_Y = 1;
                }
            }
            coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
            coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
            SetConsoleCursorPosition(hout, coord);
        }
        // 光标向其左元素移动
        if (ch == 0x4b)
        {
            UIeleSelect_X--;
            if (UIeleSelect_X <= -1)
            {
                UIeleSelect_X = 0;
            }
            coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
            coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
            SetConsoleCursorPosition(hout, coord);
        }
        // 光标向其右元素移动
        if (ch == 0x4d)
        {
            if (UIelement[UIeleSelect_Y][UIeleSelect_X].name == "管理课程")
            {
                UIeleSelect_Y = 0;
                UIeleSelect_X = 1;
            } // 自动换行
            else
            {
                UIeleSelect_X++;
                if (UIeleSelect_X >= 2)
                {
                    UIeleSelect_X = 2;
                }
            }
            coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
            coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
            SetConsoleCursorPosition(hout, coord);
        }
        // 如果在选中元素时回车
        if (ch == 0x0d)
        {
            if (UIelement[UIeleSelect_Y][UIeleSelect_X].name == "管理课程")
            {
                page = 5;
            }
            if (UIelement[UIeleSelect_Y][UIeleSelect_X].name == "所有课程")
            {
                page = 7;
            }
            if (UIelement[UIeleSelect_Y][UIeleSelect_X].name == "退出系统")
            {
                Ui.PaintConstString(0, 39, "(任意键取消)再按一次回车键退出系统");
                ch = _getch();
                if (ch != 0x0d)
                {
                    Ui.PaintConstString(0, 39, "                                           ");
                }
                else
                {
                    system("cls");
                    exit(1);
                }
            }
            if (UIelement[UIeleSelect_Y][UIeleSelect_X].name == "退出登录")
            {
                Ui.PaintConstString(0, 39, "(任意键取消)再按一次回车键退出登录");
                ch = _getch();
                if (ch != 0x0d)
                {
                    Ui.PaintConstString(0, 39, "                                           ");
                }
                else
                {
                    for (int i = 0; i < 32; i++)
                    {
                        StudentNewNote.name[i] = '\0';
                        StudentNewNote.pword[i] = '\0';
                    }
                    page = 1;
                }
            }
        }
    }
}
int User_SeleClassUI()
{
    typedef struct UIele
    {
        const char *name; // 元素的名字
        short X;          // 光标位置
        short Y;
    } UIele;
    UIele ClassList1 = {"课程1", 13, 8};
    UIele ClassList2 = {"课程2", 13, 10};
    UIele ClassList3 = {"课程3", 13, 12};
    UIele ClassList4 = {"课程4", 13, 14};
    UIele ClassList5 = {"课程5", 13, 16};
    UIele ClassList6 = {"课程6", 13, 18};
    UIele ClassList7 = {"课程7", 13, 20};
    UIele UIelement[7][1] = {ClassList1, ClassList2, ClassList3, ClassList4, ClassList5, ClassList6, ClassList7}; // 可交互元素的列表,课程的表格
    GetUserClass(Usertxt);                                                                                        // 获取用户所选课程的序号
    ReadClassData(Classtxt);                                                                                      // 获取所有课程的信息
    Ui.PaintConstString(23, 23, "您已选");
    Ui.PaintNumber(30, 23, StudentNewNote.sumofclass); // 打招呼：显示已经选课的门数
    Ui.PaintConstString(33, 23, "门课，共");
    Ui.PaintNumber(42, 23, StudentNewNote.sumofcredit); // 打招呼：显示已经选课的总学分
    Ui.PaintConstString(45, 23, "学分！");
    bouble(SchoolClassList, 7); ////按学分大小排序
    // 把数据丢到该在的位置，没有实力所以做成函数調用會出錯
    SchoolClass templist[7] = {}; // 對課表數組進行篩選並重新排序
    SchoolClass zeroList[7] = {};
    int optionalclassnb = 0;
    for (int i = 0; i < 6; i++)
    {
        templist[i] = SchoolClassList[i];
    }
    for (int i = 0; i < 6; i++)
    {

        SchoolClassList[i] = zeroList[i];
    }
    for (int i = 0; i < 6; i++)
    {
        if (CheckExistClass(i, templist, StudentNewNote) == 0 && templist[i].number != 0)
        {
            SchoolClassList[optionalclassnb] = templist[i];
            Ui.PaintNumber(13, UIelement[optionalclassnb][0].Y, templist[i].number);
            Ui.PaintConstString(19, UIelement[optionalclassnb][0].Y, templist[i].name);
            Ui.PaintConstString(41, UIelement[optionalclassnb][0].Y, templist[i].nature);
            Ui.PaintNumber(53, UIelement[optionalclassnb][0].Y, templist[i].Allhours);
            Ui.PaintNumber(61, UIelement[optionalclassnb][0].Y, templist[i].roomhours);
            Ui.PaintNumber(71, UIelement[optionalclassnb][0].Y, templist[i].esphours);
            Ui.PaintNumber(87, UIelement[optionalclassnb][0].Y, templist[i].credit);
            Ui.PaintNumber(93, UIelement[optionalclassnb][0].Y, templist[i].capacity);
            optionalclassnb++;
        }
    }
    Ui.PaintDynaString(13, 1, StudentNewNote.realname); ////////用真名欢迎
    Ui.PaintConstString(0, 39, "当前在选课界面");
    int UIeleSelect_X = 0;
    int UIeleSelect_Y = 0;
    coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
    coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
    SetConsoleCursorPosition(hout, coord); // 设置光标的初始位置在选课上                             //0x0d表示回车，0XE0表示上下左右等键的键码
    while (page == 4)
    {
        ch = _getch();
        // 光标向其上元素移动
        if (ch == 0x48)
        {
            UIeleSelect_Y--;
            if (UIeleSelect_Y <= -1)
            {
                UIeleSelect_Y = 0;
            }
            coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
            coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
            SetConsoleCursorPosition(hout, coord);
        }
        // 光标向其下元素移动
        if (ch == 0x50)
        {
            UIeleSelect_Y++;
            if (UIeleSelect_Y >= optionalclassnb)
            {
                UIeleSelect_Y = optionalclassnb - 1;
            }
            coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
            coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
            SetConsoleCursorPosition(hout, coord);
        }
        // 如果按下Esc键,退出子界面，跳转回用户界面
        if (ch == 27)
        {
            page = 2;
        }
        // 回车
        if (ch == 0x0d && optionalclassnb != 0)
        {
            EmptyClassNote();
            Ui.PaintConstString(13, 25, SchoolClassList[UIeleSelect_Y].note);
            ch = _getch();
            if (ch == 0x0d)
            {
                do
                {

                    Ui.PaintConstString(0, 39, "(Esc键取消)再按一次回车键选课:");
                    Ui.PaintDynaString(30, 39, SchoolClassList[UIeleSelect_Y].name);
                    ch = _getch();
                    Ui.PaintConstString(0, 39, "                                                      ");
                    if (ch == 27)
                    {
                        Ui.PaintConstString(0, 39, "                                                ");
                        coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
                        coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
                        SetConsoleCursorPosition(hout, coord);
                        EmptyClassNote(); // 清空课程简介
                    }
                    if (ch == 0x0d)
                    { // 选课要求判断
                        if ((StudentNewNote.sumofclass + 1 < 4) && ((StudentNewNote.sumofcredit + SchoolClassList[UIeleSelect_Y].credit) <= 10))
                        {
                            AddUserClass(StudentNewNote.name, SchoolClassList[UIeleSelect_Y].number, Usertxt, SchoolClassList[UIeleSelect_Y].credit);
                            // PaintDynaString(0, 33, Usertxt);
                            MessageBox(0, TEXT("您的选课已经成功"), TEXT("选课成功"), 0);
                            UIeleSelect_Y--;
                            if (UIeleSelect_Y <= -1)
                            {
                                UIeleSelect_Y = 0;
                            }
                            coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
                            coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
                            SetConsoleCursorPosition(hout, coord);
                            EmptyClassNote(); // 清空课程简介
                            for (int i = 0; i < 6; i++)
                            {
                                StudentNewNote.classlist[i] = -1;
                            }
                            return 1;
                        }
                        else if (StudentNewNote.sumofclass + 1 >= 4)
                        {
                            MessageBox(0, TEXT("选课不能超过4门"), TEXT("选课失败"), 0);
                            coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
                            coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
                            SetConsoleCursorPosition(hout, coord);
                            EmptyClassNote(); // 清空课程简介
                        }
                        else if (StudentNewNote.sumofcredit + SchoolClassList[UIeleSelect_Y].credit > 10)
                        {
                            MessageBox(0, TEXT("选课失败"), TEXT("选课总学分不能超过10"), 0);
                            coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
                            coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
                            SetConsoleCursorPosition(hout, coord);
                            EmptyClassNote(); // 清空课程简介
                        }
                    }
                } while (ch != 27 && ch != 0x0d);
            }
        }
    }
    return 0;
}
int Root_ManageClassUI()
{
    typedef struct UIele
    {
        const char *name; // 元素的名字
        short X;          // 光标位置
        short Y;
    } UIele;
    UIele ClassList1 = {"课程1", 13, 8};
    UIele ClassList2 = {"课程2", 13, 10};
    UIele ClassList3 = {"课程3", 13, 12};
    UIele ClassList4 = {"课程4", 13, 14};
    UIele ClassList5 = {"课程5", 13, 16};
    UIele ClassList6 = {"课程6", 13, 18};
    UIele ClassList7 = {"课程7", 13, 20};
    UIele UIelement[7][1] = {ClassList1, ClassList2, ClassList3, ClassList4, ClassList5, ClassList6, ClassList7}; // 可交互元素的列表,课程的表格
    ReadClassData(Classtxt);                                                                                      // 获取所有课程的信息
    bouble(SchoolClassList, 7);                                                                                   ////按学分大小排序
    int AllClassNum;
    for (AllClassNum = 0; SchoolClassList[AllClassNum].number != 0; AllClassNum++)
    {
    }
    for (int i = 0; i < AllClassNum; i++) ///////////把数据丢到该在的位置，没有实力所以做不成函数
    {

        Ui.PaintNumber(13, UIelement[i][0].Y, SchoolClassList[i].number);
        Ui.PaintConstString(19, UIelement[i][0].Y, SchoolClassList[i].name);
        Ui.PaintConstString(41, UIelement[i][0].Y, SchoolClassList[i].nature);
        Ui.PaintNumber(53, UIelement[i][0].Y, SchoolClassList[i].Allhours);
        Ui.PaintNumber(61, UIelement[i][0].Y, SchoolClassList[i].roomhours);
        Ui.PaintNumber(71, UIelement[i][0].Y, SchoolClassList[i].esphours);
        Ui.PaintNumber(87, UIelement[i][0].Y, SchoolClassList[i].credit);
        Ui.PaintNumber(93, UIelement[i][0].Y, SchoolClassList[i].capacity);
    }
    Ui.PaintDynaString(13, 1, StudentNewNote.realname); ////////用真名欢迎
    int UIeleSelect_X = 0;
    int UIeleSelect_Y = 0;
    coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
    coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
    SetConsoleCursorPosition(hout, coord); // 设置光标的初始位置在选课上                             //0x0d表示回车，0XE0表示上下左右等键的键码
    while (page == 5)
    {
        ch = _getch();

        // 光标向其上元素移动
        if (ch == 0x48)
        {
            UIeleSelect_Y--;
            if (UIeleSelect_Y <= -1)
            {
                UIeleSelect_Y = 0;
            }
            coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
            coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
            SetConsoleCursorPosition(hout, coord);
        }
        // 光标向其下元素移动
        if (ch == 0x50)
        {
            UIeleSelect_Y++;
            if (UIeleSelect_Y >= AllClassNum)
            {
                UIeleSelect_Y = AllClassNum - 1;
            }
            if (AllClassNum == 0)
            {
                UIeleSelect_Y = 0;
            }
            coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
            coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
            SetConsoleCursorPosition(hout, coord);
        }
        // 如果按下Esc键,退出子界面，跳转回用户界面
        if (ch == 27)
        {
            page = 3;
        }
        // 如果在选中元素时回车
        if (ch == 0x0d)
        {
            EmptyClassNote();
            Ui.PaintConstString(13, 25, SchoolClassList[UIeleSelect_Y].note);
            Ui.PaintConstString(0, 39, "再按一次回车键修改课程:");
            Ui.PaintDynaString(0, 59, SchoolClassList[UIeleSelect_Y].name);
            coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
            coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
            SetConsoleCursorPosition(hout, coord);
            ch = _getch();
            Ui.PaintConstString(0, 39, "                               ");

            if (ch == 0x0d)
            {
                page = 8;
                while (page == 8)
                {
                    EmptyClassNote();
                    CreatRootUI_Son_EditClass();
                    Root_EditClassUI(UIeleSelect_Y);
                }
                return 1;
            }
            if (ch != 0x0d)
            {
                EmptyClassNote(); // 清空课程简介
            }
        }
    }
    return 0;
}
void Root_AllClassUI()
{
    typedef struct UIele
    {
        const char *name; // 元素的名字
        short X;          // 光标位置
        short Y;
    } UIele;
    UIele ClassList1 = {"课程1", 13, 8};
    UIele ClassList2 = {"课程2", 13, 10};
    UIele ClassList3 = {"课程3", 13, 12};
    UIele ClassList4 = {"课程4", 13, 14};
    UIele ClassList5 = {"课程5", 13, 16};
    UIele ClassList6 = {"课程6", 13, 18};
    UIele ClassList7 = {"课程7", 13, 20};
    UIele UIelement[7][1] = {ClassList1, ClassList2, ClassList3, ClassList4, ClassList5, ClassList6, ClassList7}; // 可交互元素的列表,课程的表格
    ReadClassData(Classtxt);                                                                                      // 获取所有课程的信息
    bouble(SchoolClassList, 7);                                                                                   ////按学分大小排序
    int AllClassNum;
    for (AllClassNum = 0; SchoolClassList[AllClassNum].number != 0; AllClassNum++)
    {
    }
    for (int i = 0; i < AllClassNum; i++) ///////////把数据丢到该在的位置，没有实力所以做不成函数
    {

        Ui.PaintNumber(13, UIelement[i][0].Y, SchoolClassList[i].number);
        Ui.PaintConstString(19, UIelement[i][0].Y, SchoolClassList[i].name);
        Ui.PaintConstString(41, UIelement[i][0].Y, SchoolClassList[i].nature);
        Ui.PaintNumber(53, UIelement[i][0].Y, SchoolClassList[i].Allhours);
        Ui.PaintNumber(61, UIelement[i][0].Y, SchoolClassList[i].roomhours);
        Ui.PaintNumber(71, UIelement[i][0].Y, SchoolClassList[i].esphours);
        Ui.PaintNumber(87, UIelement[i][0].Y, SchoolClassList[i].credit);
        Ui.PaintNumber(93, UIelement[i][0].Y, SchoolClassList[i].capacity);
    }
    Ui.PaintDynaString(13, 1, StudentNewNote.realname); ////////用真名欢迎
    Ui.PaintConstString(0, 39, "1111111111111");
    int UIeleSelect_X = 0;
    int UIeleSelect_Y = 0;
    coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
    coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
    SetConsoleCursorPosition(hout, coord); // 设置光标的初始位置在选课上                             //0x0d表示回车，0XE0表示上下左右等键的键码
    while (page == 7)
    {
        ch = _getch();
        // 光标向其上元素移动
        if (ch == 0x48)
        {
            UIeleSelect_Y--;
            if (UIeleSelect_Y <= -1)
            {
                UIeleSelect_Y = 0;
            }
            coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
            coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
            SetConsoleCursorPosition(hout, coord);
        }
        // 光标向其下元素移动
        if (ch == 0x50)
        {
            UIeleSelect_Y++;
            if (UIeleSelect_Y >= AllClassNum)
            {
                UIeleSelect_Y = AllClassNum - 1;
            }
            if (AllClassNum == 0)
            {
                UIeleSelect_Y = 0;
            }
            coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
            coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
            SetConsoleCursorPosition(hout, coord);
        }

        // 如果按下Esc键,退出子界面，跳转回管理员界面
        if (ch == 27)
        {
            page = 3;
        }
        // 如果在选中元素时回车
        if (ch == 0x0d)
        {
            ReadClassSelecter(SchoolClassList[UIeleSelect_Y].number, Usertxt, ClassSelecter);
            Ui.PaintConstString(13, 25, "该课的学生有：");
            int k = 0; // 名单的横向偏移量
            int j = 0; // 名单的纵向偏移量
            for (int i = 0; i < 16; i++)
            {

                if (ClassSelecter[i][0] != '\0') // 因为ReadClassSelecter得出的数组是有‘\0’间隔的，所有需要判断有内容从打印出来
                {

                    Ui.PaintDynaString(13 + k * 8, 26, &ClassSelecter[i][0]);
                    k++;
                    if (13 + k * 8 > 90)
                    {
                        k = 0;
                        j++;
                    }
                }
            }
            coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
            coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
            SetConsoleCursorPosition(hout, coord);
            ch = _getch();
            if (ch == 27)
            {
                EmptyClassNote(); // 清空课程简介
                for (int i = 0; i < 16; i++)
                {
                    for (int j = 0; j < 16; j++)
                    {
                        ClassSelecter[i][j] = '\0';
                    }
                }
            }
            if (ch != 0x0d)
            {
                EmptyClassNote(); // 清空课程简介
                for (int i = 0; i < 16; i++)
                {
                    for (int j = 0; j < 16; j++)
                    {
                        ClassSelecter[i][j] = '\0';
                    }
                }
            }
        }
    }
}
int Root_EditClassUI(int nbinlist)
{
    typedef struct UIele
    {
        const char *name; // 元素的名字
        short X;          // 光标位置
        short Y;
    } UIele;
    UIele number = {"编号", 13, 27};
    UIele name = {"名称", 19, 27};
    UIele nature = {"性质", 41, 27};
    UIele Allhours = {"总学时", 53, 27};
    UIele roomhours = {"授课学时", 61, 27};
    UIele esphours = {"实验或上机学时", 71, 27};
    UIele credit = {"学分", 87, 27};
    UIele capacity = {" 容量", 93, 27};
    UIele note = {" 内容", 13, 29};
    UIele UIelement[1][9] = {number, name, nature, Allhours, roomhours, esphours, credit, capacity, note}; // 可交互元素的列表,课程的表格
    Ui.PaintConstString(0, 39, "                  ");
    ReadClassData(Classtxt);    // 刷新课程信息
    bouble(SchoolClassList, 7); ////按学分大小排序
    int UIeleSelect_X = 0;
    int UIeleSelect_Y = 0;
    coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
    coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
    SetConsoleCursorPosition(hout, coord); // 设置光标的初始位置在选课上                             //0x0d表示回车，0XE0表示上下左右等键的键码
    while (page == 8)
    {
        Ui.PaintNumber(13, 27, SchoolClassList[nbinlist].number);
        Ui.PaintConstString(19, 27, SchoolClassList[nbinlist].name);
        Ui.PaintConstString(41, 27, SchoolClassList[nbinlist].nature);
        Ui.PaintNumber(53, 27, SchoolClassList[nbinlist].Allhours);
        Ui.PaintNumber(61, 27, SchoolClassList[nbinlist].roomhours);
        Ui.PaintNumber(71, 27, SchoolClassList[nbinlist].esphours);
        Ui.PaintNumber(87, 27, SchoolClassList[nbinlist].credit);
        Ui.PaintNumber(93, 27, SchoolClassList[nbinlist].capacity);
        Ui.PaintConstString(13, 30, SchoolClassList[nbinlist].note);
        coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
        coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
        SetConsoleCursorPosition(hout, coord); // 设置光标的初始位置在选课上                             //0x0d表示回车，0XE0表示上下左右等键的键码
        ch = _getch();
        // 光标向其左元素移动
        if (ch == 0x4b)
        {
            UIeleSelect_X--;
            if (UIeleSelect_X <= -1)
            {
                UIeleSelect_X = 0;
            }
            coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
            coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
            SetConsoleCursorPosition(hout, coord);
        }
        // 光标向其右元素移动
        if (ch == 0x4d)
        {

            UIeleSelect_X++;
            if (UIeleSelect_X >= 9)
            {
                UIeleSelect_X = 8;
            }
            coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
            coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
            SetConsoleCursorPosition(hout, coord);
        }
        // 光标向其上元素移动

        // 如果按下Esc键,退出子界面，跳转回管理员界面
        if (ch == 27)
        {
            page = 5;
        }
        // 如果在选中元素时回车
        if (ch == 0x0d)
        {
            Ui.PaintConstString(0, 39, "再按一次回车键修改:");
            coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
            coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
            SetConsoleCursorPosition(hout, coord);
            ch = _getch();
            if (ch == 0x0d)
            {
                if (UIelement[0][UIeleSelect_X].name != "编号")
                {
                    Ui.PaintConstString(17, 23, UIelement[0][UIeleSelect_X].name);
                    Ui.PaintConstString(31, 23, "→ ");
                    char EditNote[32] = {};
                    if (UIelement[0][UIeleSelect_X].name != "名称" && UIelement[0][UIeleSelect_X].name != "性质" && UIelement[0][UIeleSelect_X].name != "内容")
                    {
                        Ui.PaintConstString(0, 39, "纯数字内容请以两位格式输入例：08");
                        Ui.PaintConstString(31, 23, "→ ");
                        gets(EditNote);
                    }
                    else
                    {
                        gets(EditNote);
                    }
                    EditClass(SchoolClassList[nbinlist].number, UIeleSelect_X, Classtxt, EditNote);
                    Ui.PaintConstString(0, 39, "                                                  ");
                    return 1;
                }
                else
                {
                    Ui.PaintConstString(0, 39, "请勿修改序号                        ");
                    ch = _getch();
                    return 0;
                }
            }
            coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
            coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
            SetConsoleCursorPosition(hout, coord);
        }
    }
    return 0;
}
int User_MyClassUI()
{
    typedef struct UIele
    {
        const char *name; // 元素的名字
        short X;          // 光标位置
        short Y;
        student linkstdent;

    } UIele;
    UIele ClassList1 = {"课程1", 13, 8};
    UIele ClassList2 = {"课程2", 13, 10};
    UIele ClassList3 = {"课程3", 13, 12};
    UIele ClassList4 = {"课程4", 13, 14};
    UIele ClassList5 = {"课程5", 13, 16};
    UIele ClassList6 = {"课程6", 13, 18};
    UIele ClassList7 = {"课程7", 13, 20};
    UIele UIelement[7][1] = {ClassList1, ClassList2, ClassList3, ClassList4, ClassList5, ClassList6, ClassList7}; // 可交互元素的列表,课程的表格
    GetUserClass(Usertxt);                                                                                        // 获取用户所选课程的序号
    ReadClassData(Classtxt);                                                                                      // 获取所有课程的信息
    bouble(SchoolClassList, 7);                                                                                   ////按学分大小排序
    Ui.PaintConstString(23, 23, "您已选");
    Ui.PaintNumber(30, 23, StudentNewNote.sumofclass); // 打招呼：显示已经选课的门数
    Ui.PaintConstString(33, 23, "门课，共");
    Ui.PaintNumber(42, 23, StudentNewNote.sumofcredit); // 打招呼：显示已经选课的总学分
    Ui.PaintConstString(45, 23, "学分！");
    // 把数据丢到该在的位置，没有实力所以做成函数調用會出錯
    SchoolClass templist[7] = {}; // 對課表數組進行篩選並重新排序
    SchoolClass zeroList[7] = {};
    int myclassnb = 0;
    for (int i = 0; i < 6; i++)
    {
        templist[i] = SchoolClassList[i];
    }
    for (int i = 0; i < 6; i++)
    {

        SchoolClassList[i] = zeroList[i];
    }
    for (int i = 0; i < 6; i++)
    {
        if (CheckExistClass(i, templist, StudentNewNote) == 1) // 筛选课程
        {
            SchoolClassList[myclassnb] = templist[i];
            Ui.PaintNumber(13, UIelement[myclassnb][0].Y, templist[i].number);
            Ui.PaintConstString(19, UIelement[myclassnb][0].Y, templist[i].name);
            Ui.PaintConstString(41, UIelement[myclassnb][0].Y, templist[i].nature);
            Ui.PaintNumber(53, UIelement[myclassnb][0].Y, templist[i].Allhours);
            Ui.PaintNumber(61, UIelement[myclassnb][0].Y, templist[i].roomhours);
            Ui.PaintNumber(71, UIelement[myclassnb][0].Y, templist[i].esphours);
            Ui.PaintNumber(87, UIelement[myclassnb][0].Y, templist[i].credit);
            Ui.PaintNumber(93, UIelement[myclassnb][0].Y, templist[i].capacity);
            myclassnb++;
        }
    }
    Ui.PaintDynaString(13, 1, StudentNewNote.realname); ////////用真名欢迎
    Ui.PaintConstString(0, 39, "当前在我的课程界面");
    int UIeleSelect_X = 0;
    int UIeleSelect_Y = 0;
    coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
    coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
    SetConsoleCursorPosition(hout, coord); // 设置光标的初始位置在选课上                             //0x0d表示回车，0XE0表示上下左右等键的键码
    while (page == 6)
    {
        ch = _getch();
        // 光标向其上元素移动
        if (ch == 0x48)
        {
            UIeleSelect_Y--;
            if (UIeleSelect_Y <= -1)
            {
                UIeleSelect_Y = 0;
            }
            coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
            coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
            SetConsoleCursorPosition(hout, coord);
        }
        // 光标向其下元素移动
        if (ch == 0x50)
        {
            UIeleSelect_Y++;
            if (UIeleSelect_Y >= myclassnb)
            {
                UIeleSelect_Y = myclassnb - 1;
            }
            coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
            coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
            SetConsoleCursorPosition(hout, coord);
        }
        // 如果按下Esc键,退出子界面，跳转回用户界面
        if (ch == 27)
        {
            page = 2;
        }
        // 回车
        if (ch == 0x0d)
        {
            EmptyClassNote();
            Ui.PaintConstString(13, 25, SchoolClassList[UIeleSelect_Y].note);
            ch = _getch();
            if (ch == 0x0d)
            {
                do
                {
                    Ui.PaintConstString(13, 25, SchoolClassList[UIeleSelect_Y].note);
                    Ui.PaintConstString(0, 39, "(Esc键取消)再按一次回车键退课:");
                    Ui.PaintDynaString(30, 39, SchoolClassList[UIeleSelect_Y].name);
                    // Ui.PaintConstString(0, 39, "Warning");
                    ch = _getch();
                    Ui.PaintConstString(0, 39, "                                                      ");
                    if (ch == 27)
                    {
                        Ui.PaintConstString(0, 39, "                                                ");
                        coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
                        coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
                        SetConsoleCursorPosition(hout, coord);
                        EmptyClassNote(); // 清空课程简介
                    }
                    if (StudentNewNote.sumofclass - 1 >= 2)
                    {
                        if (ch == 0x0d)
                        {
                            DeleteUserClass(StudentNewNote.name, SchoolClassList[UIeleSelect_Y].number, Usertxt, SchoolClassList[UIeleSelect_Y].credit);
                            // PaintDynaString(0, 33, Usertxt);
                            Ui.PaintConstString(0, 39, "退课:");
                            Ui.PaintDynaString(0, 44, SchoolClassList[UIeleSelect_Y].name);
                            Ui.PaintConstString(0, 44 + sizeof(SchoolClassList[UIeleSelect_Y].name), "成功");
                            UIeleSelect_Y--;
                            if (UIeleSelect_Y <= -1)
                            {
                                UIeleSelect_Y = 0;
                            }
                            coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
                            coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
                            SetConsoleCursorPosition(hout, coord);
                            EmptyClassNote(); // 清空课程简介
                            for (int i = 0; i < 6; i++)
                            {
                                StudentNewNote.classlist[i] = -1;
                            }
                            return 1;
                        }
                    }
                    else if (StudentNewNote.sumofclass - 1 < 2)
                    {
                        MessageBox(0, TEXT("选课不能小于2门"), TEXT("选课失败"), 0);
                        coord.X = UIelement[UIeleSelect_Y][UIeleSelect_X].X;
                        coord.Y = UIelement[UIeleSelect_Y][UIeleSelect_X].Y;
                        SetConsoleCursorPosition(hout, coord);
                        EmptyClassNote(); // 清空课程简介
                    }
                } while (ch != 27 && ch != 0x0d);
            }
        }
    }
    return 0;
}
void bouble(struct SchoolClass array[], int n) ////冒泡排序，大的上浮
{
    int i, j;
    struct SchoolClass t;
    for (i = 1; i < n; i++)
    {
        for (j = 0; j < n - i; j++)
        {
            if (array[j].credit < array[j + 1].credit)
            {
                t = array[j];
                array[j] = array[j + 1];
                array[j + 1] = t;
            }
        }
    }
}

void EmptyClassNote()
{
    for (int i = 0; i < 12; i++)
    {
        Ui.PaintConstString(13, i + 25, "                                                                                     ");
    }
}